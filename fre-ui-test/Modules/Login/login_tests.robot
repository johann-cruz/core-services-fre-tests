*** Settings ***
Library             SeleniumLibrary
Resource            ../../Resources/Commons/resources.robot
Resource            ../../Resources/TestData/config_data.robot
Resource            ../../Resources/Steps/login_steps.robot
Resource            ../../Resources/Steps/forecast_steps.robot
Resource            ../../Resources/Steps/forgotpassword_steps.robot

Test Setup         resources.Start TestCase
Test Teardown      resources.Finish TestCase

*** Test Cases ***
Invalid Login
    GIVEN the user opens login Page
    WHEN the user inputs credentials    ${email_superadmin}     ${password_invalid}
    THEN the user will be able to see error message

Verify that the user can able to forgot password
    GIVEN the user opens login Page
    WHEN the user forgets password     ${dummy_email} 
    THEN the user will be able to see email verification    ${dummy_email}

Verify that the user successfully logs into the Offshore Forecasts app
    GIVEN the user opens login Page
    WHEN the user inputs credentials    ${email_superadmin}     ${password_valid}
    THEN the user will be able to access Forecast Page