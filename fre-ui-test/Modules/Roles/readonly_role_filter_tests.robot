*** Settings ***
Library             SeleniumLibrary
Resource            ../../Resources/Commons/resources.robot
Resource            ../../Resources/TestData/config_data.robot
Resource            ../../Resources/Steps/login_steps.robot
Resource            ../../Resources/Steps/forecast_steps.robot
Resource            ../../Resources/Steps/forecast_filter_steps.robot

Test Setup         resources.Start TestCase
Test Teardown      resources.Finish TestCase

*** Variables ***
# Sort by
${filter_modal_expiring_first_label}                       xpath: //span[normalize-space()='Expiring first']
${filter_modal_name_a_to_z_label}                          xpath: //span[normalize-space()='Name: A to Z']    
${filter_modal_name_z_to_a_label}                          xpath: //span[normalize-space()='Name: Z to A']
${filter_modal_newest_first_label}                         xpath: //span[normalize-space()='Newest first']
${filter_modal_oldest_first_label}                         xpath: //span[normalize-space()='Oldest first']

# Status
${filter_modal_all_statuses_label}                         xpath: //span[normalize-space()='All Statuses']
${filter_modal_actual_label}                               xpath: //span[normalize-space()='Actual']
${filter_modal_stopped_label}                              xpath: //span[@role='button'][normalize-space()='Stopped']
${filter_modal_obsolete_label}                             css: div:nth-of-type(3) > span:nth-of-type(4)
${filter_modal_coming_soon_label}                          xpath: //span[normalize-space()='Coming soon']
${filter_modal_configuration_required_label}               css: div:nth-of-type(3) > span:nth-of-type(6)
${filter_modal_route_calculation_in_progress_label}        xpath: //span[normalize-space()='Route calculation in progress']

# Forecast Admin
${filter_modal_missing_financial_information_value}        Missing financial information
${filter_modal_unverified_financial_information_value}     Unverified financial information

# Filter Button
${filter_button_value}             FILTER
${filter_button_value1}            FILTER (1)
${filter_button_value2}            FILTER (2)
${filter_button_value3}            FILTER (3)

*** Test Cases ***
Verify readOnly role can sort and filter - Expiring first - Combinations
    GIVEN the user logins successfully    ${email_readonly}                       ${password_valid}
    # All Statuses
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}    ${filter_modal_unverified_financial_information_value}     
    # Actual
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_actual_label}                           ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_actual_label}                           ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_actual_label}                           ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Stopped
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Obsolete
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Coming Soon
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Configuration required
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Route calculation in progress
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_expiring_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 

Verify readOnly role can sort and filter - Name: A to Z - Combinations
    GIVEN the user logins successfully    ${email_readonly}                    ${password_valid}
    # All Statuses
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Actual
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_actual_label}                           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Stopped
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Obsolete
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Coming Soon
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Configuration required
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Route calculation in progress
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_a_to_z_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 

Verify readOnly role can sort and filter - Name: Z to A - Combinations
    GIVEN the user logins successfully    ${email_readonly}                    ${password_valid}
    # All Statuses
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Actual
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_actual_label}                           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Stopped
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Obsolete
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Coming Soon
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Configuration required
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Route calculation in progress
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_name_z_to_a_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 

Verify readOnly role can sort and filter - Newest first - Combinations
    GIVEN the user logins successfully    ${email_readonly}                     ${password_valid}
    # All Statuses
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Actual
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Stopped
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Obsolete
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Coming Soon
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Configuration required
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Route calculation in progress
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_newest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 

Verify readOnly role can sort and filter - Oldest first - Combinations
    GIVEN the user logins successfully    ${email_readonly}                     ${password_valid}
    # All Statuses
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value1}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_all_statuses_label}                     ${filter_button_value2}    ${filter_modal_unverified_financial_information_value} 
    # Actual
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_actual_label}                           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Stopped
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_stopped_label}                          ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Obsolete
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_obsolete_label}                         ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Coming Soon
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_coming_soon_label}                      ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Configuration required
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_configuration_required_label}           ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 
    # Route calculation in progress
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value2}                                                   
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_missing_financial_information_value}        
    Filter Scenarios                     ${filter_modal_oldest_first_label}    ${filter_modal_route_calculation_in_progress_label}    ${filter_button_value3}    ${filter_modal_unverified_financial_information_value} 


*** Keywords ***
Filter Scenarios
    [Arguments]    ${sortby}    ${status}    ${filter_text}    ${forecast_admin}=${None}    
    WHEN the user applies sort and filter                   ${sortby}     ${status}    ${forecast_admin}
    THEN the user will able to check filter button label    ${filter_text}