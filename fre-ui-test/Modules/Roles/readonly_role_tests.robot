*** Settings ***
Library             SeleniumLibrary
Resource            ../../Resources/Commons/resources.robot
Resource            ../../Resources/TestData/config_data.robot
Resource            ../../Resources/Steps/login_steps.robot
Resource            ../../Resources/Steps/forecast_steps.robot
Resource            ../../Resources/Steps/forecast_filter_steps.robot
Resource            ../../Resources/Steps/forecast_indepth_admin_steps.robot
Resource            ../../Resources/Steps/forecast_indepth_steps.robot

Test Setup         resources.Start TestCase
Test Teardown      resources.Finish TestCase

*** Variables ***
# Sort by
${filter_modal_expiring_first_label}                       xpath: //span[normalize-space()='Expiring first']
${filter_modal_name_a_to_z_label}                          xpath: //span[normalize-space()='Name: A to Z']    
${filter_modal_name_z_to_a_label}                          xpath: //span[normalize-space()='Name: Z to A']
${filter_modal_newest_first_label}                         xpath: //span[normalize-space()='Newest first']
${filter_modal_oldest_first_label}                         xpath: //span[normalize-space()='Oldest first']

# Status
${filter_modal_all_statuses_label}                         xpath: //span[normalize-space()='All Statuses']
${filter_modal_actual_label}                               xpath: //span[normalize-space()='Actual']
${filter_modal_stopped_label}                              xpath: //span[@role='button'][normalize-space()='Stopped']
${filter_modal_obsolete_label}                             css: div:nth-of-type(3) > span:nth-of-type(4)
${filter_modal_coming_soon_label}                          xpath: //span[normalize-space()='Coming soon']
${filter_modal_configuration_required_label}               css: div:nth-of-type(3) > span:nth-of-type(6)
${filter_modal_route_calculation_in_progress_label}        xpath: //span[normalize-space()='Route calculation in progress']

# Forecast Admin
${filter_modal_missing_financial_information_value}        Missing financial information
${filter_modal_unverified_financial_information_value}     Unverified financial information

# Filter Button
${filter_button_value}             FILTER
${filter_button_value1}            FILTER (1)
${filter_button_value2}            FILTER (2)
${filter_button_value3}            FILTER (3)

*** Test Cases ***
Verify readonly role has View option in the forecast context menu
    GIVEN the user logins successfully    ${email_readonly}      ${password_valid}
    WHEN the user checks forecast context menu
    THEN the user will only see View option

Verify readonly role can open sort and filter
    GIVEN the user logins successfully    ${email_readonly}      ${password_valid}
    WHEN the user opens filter button
    THEN the user will able to verify sort and filter objects

Verify readonly role cannot see Forecast Add button
    GIVEN the user logins successfully    ${email_readonly}      ${password_valid}
    WHEN the user will be able to access Forecast Page
    THEN the user will not see forecast add button

Verify readonly role can see forecast finance details
    GIVEN the user logins successfully    ${email_readonly}      ${password_valid}
    WHEN the user clicks forecast link
    THEN the user will able to see forecast admin

Verify readonly role cannot see forecast admin link
    GIVEN the user logins successfully    ${email_readonly}      ${password_valid}
    WHEN the user clicks forecast link
    the user will not able to see the forecast admin link