*** Settings ***
Resource    ../PageObjects/login_page.robot
Resource    ../PageObjects/forgotpassword_page.robot

*** Keywords ***
the user forgets password
    [Arguments]    ${email}
    login_page.Click Forgot Password link
    forgotpassword_page.Input Email     ${email}
    forgotpassword_page.Click Continue Button

the user will be able to see email verification
    [Arguments]    ${email}
    forgotpassword_page.Verify Email    ${email}