*** Settings ***
Resource    ../PageObjects/login_page.robot

*** Keywords ***
the user opens login Page
    login_page.Login Page Opened

the user inputs credentials
    [Arguments]     ${email}     ${password}
    login_page.Input Email       ${email}
    login_page.Input Password    ${password}
    login_page.Click Continue Button

the user will be able to see error message
    login_page.Verify Error Message

the user logins successfully
    [Arguments]     ${email}    ${password}
    the user opens login Page
    the user inputs credentials     ${email}     ${password}