*** Settings ***
Resource    ../PageObjects/forecast_page.robot

*** Keywords ***
the user will be able to access Forecast Page
    forecast_page.Verify Forecast Header
    forecast_page.Verify Forecast URL

the user checks forecast context menu
    forecast_page.Verify Forecast First Tile
    forecast_page.Hover Forecast First Tile
    forecast_page.Click Forecast Ellipsis

the user will only see View option
    forecast_page.Verify View
    forecast_page.Verify ReadOnly User Ellipsis Menu

the user will able to check filter button label 
    [Arguments]    ${filter_button_value}
    forecast_page.Verify Filter Button
    forecast_page.Verify Filter Btn Label    ${filter_button_value}

the user opens filter button
    forecast_page.Verify Filter Button
    forecast_page.Click Filter Button

the user will not see forecast add button
    forecast_page.Verify Forecast Add Button Is Not Present

the user clicks forecast link
    forecast_page.Verify Forecast First Tile
    forecast_page.Click Forecast link