*** Settings ***
Resource    ../PageObjects/forecast_indepth_admin_page.robot

*** Keywords ***
the user will able to see forecast admin
    forecast_indepth_admin_page.Verify Forecast Admin Header
    forecast_indepth_admin_page.Verify Customer ID
    forecast_indepth_admin_page.Verify Customer Name
    forecast_indepth_admin_page.Verify Type Of Contract
    forecast_indepth_admin_page.Verify Subscription Number
    forecast_indepth_admin_page.Verify Product Code
    forecast_indepth_admin_page.Verify Project Description
    forecast_indepth_admin_page.Verify Project ID
    forecast_indepth_admin_page.Verify Relates To Project
    forecast_indepth_admin_page.Verify Billing Status
    forecast_indepth_admin_page.Verify SF Contract ID
    forecast_indepth_admin_page.Verify Type Of SF Contract
    forecast_indepth_admin_page.Verify Contract Start Date
    forecast_indepth_admin_page.Verify Contract End Date
    forecast_indepth_admin_page.Verify ERP Reference
    forecast_indepth_admin_page.Verify Billing Entity Of Customer
    forecast_indepth_admin_page.Verify PO Number
    forecast_indepth_admin_page.Verify Customer Contact
    forecast_indepth_admin_page.Verify Invoicing Method
    forecast_indepth_admin_page.Verify Billing Entity
    forecast_indepth_admin_page.Verify Price
    forecast_indepth_admin_page.Verify Currency
    forecast_indepth_admin_page.Verify Cross Sell With DTN LLC
    forecast_indepth_admin_page.Verify IMO Number With Filled