*** Settings ***
Resource    ../PageObjects/forecast_filter_page.robot
Resource    ../PageObjects/forecast_page.robot

*** Keywords ***
the user will able to verify sort and filter objects
    forecast_filter_page.Verify Sort and Filter Header
    forecast_filter_page.Verify Sort By Header
    forecast_filter_page.Verify Expiring First
    forecast_filter_page.Verify Name A to Z
    forecast_filter_page.Verify Name Z to A
    forecast_filter_page.Verify Newest First
    forecast_filter_page.Verify Oldest First
    forecast_filter_page.Verify Status Header
    forecast_filter_page.Verify All Statuses
    forecast_filter_page.Verify Actual
    forecast_filter_page.Verify Stopped
    forecast_filter_page.Verify Obsolete
    forecast_filter_page.Verify Coming soon
    forecast_filter_page.Verify Configuration required
    forecast_filter_page.Verify Route Calculation In Progress
    forecast_filter_page.Verify Forecast Admin Header
    forecast_filter_page.Verify Missing Financial Information
    forecast_filter_page.Verify Unverified Financial Information
    forecast_filter_page.Verify Apply Button
    forecast_filter_page.Verify Close Button
    forecast_filter_page.Click Close Button

the user applies sort and filter
    [Arguments]       ${sort_by_filter}    ${status_filter}    ${forecast_admin_filter}=${None}
    forecast_page.Verify Filter Button
    forecast_page.Click Filter Button
    forecast_filter_page.Click Sort And Filter    ${sort_by_filter}    ${status_filter}    ${forecast_admin_filter}