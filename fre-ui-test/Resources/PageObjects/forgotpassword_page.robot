*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${email_fields}                       xpath: /html//input[@id='email']
${continue_button}                    xpath: //button[normalize-space()='Continue']
${check_your_email_label}             xpath: //h3[normalize-space()='Check Your Email']
${check_your_email_value}             Check Your Email
${check_your_email_address_label}     xpath: //div[contains(text(),'Please check the email address')]
${check_your_email_address_value1}    Please check the email address
${check_your_email_address_value2}    for instructions to reset your password.

*** Keywords ***
#############################
### Verify objects exists ###
#############################

Verify Email
    [Arguments]        ${email}           
    Wait Until Element Is Enabled    ${check_your_email_label}
    Element Text Should Be           ${check_your_email_label}            ${check_your_email_value}

    Wait Until Element Is Enabled    ${check_your_email_address_label}
    Element Text Should Be           ${check_your_email_address_label}    ${check_your_email_address_value1}${space}${email}${space}${check_your_email_address_value2}

###############
### Actions ###
###############

Input Email 
    [Arguments]                      ${email}
    Wait Until Element Is Enabled    ${email_fields}
    Input Text                       ${email_fields}                      ${email}

Click Continue Button
    Wait Until Element Is Enabled    ${continue_button}
    Click Element                    ${continue_button}