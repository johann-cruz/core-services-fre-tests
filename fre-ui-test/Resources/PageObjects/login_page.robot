*** Settings ***
Library     SeleniumLibrary
Resource    ../TestData/config_data.robot

*** Variables ***
${email_field}             id: username
${password_field}          id: password
${continue_button}         xpath: //button[normalize-space()='Continue']
${error_label}             xpath: //li[contains(text(),'Wrong email or password')]
${error_label_value}       Wrong email or password 
${forgot_password_label}   xpath: //a[normalize-space()='Forgot password?']
${logo}                    id: prompt-logo-center
${welcome_label}           xpath: //h1[contains(text(),'Welcome')]
${welcome_label_value}     Welcome
${login_label}             xpath: //p[contains(text(),'Log in to DTN Offshore Flexible Reporting')]
${login_label_value1}      Log in to DTN Offshore Flexible Reporting (
${login_label_value2}      ) to continue to Flexible Report Engine UI — Offshore (
${login_label_value3}      ).
${login_label_value4}      ) to continue to Flexible Report Engine UI — Offshore.
${test_value}              TEST
${prod_value}              PROD

*** Keywords ***
#############################
### Verify objects exists ###
#############################

Verify Logo
    Wait Until Element Is Visible    ${logo}
    Element Should Be Visible        ${logo}

Verify Welcome Message
    Wait Until Element Is Visible    ${welcome_label}
    Element Text Should Be           ${welcome_label}    Welcome

Verify Login Label
    Wait Until Element Is Visible    ${login_label}
    Run Keyword If                   '${env}' == '${test_value}'      Verify Login Label Value in Test Environment
    ...  ELSE                        Verify Login Label Value in Prod Environment      

Verify Login Label Value in Test Environment
    Element Text Should Be           ${login_label}    ${login_label_value1}${env}${login_label_value2}${env}${login_label_value3}

Verify Login Label Value in Prod Environment
    Element Text Should Be           ${login_label}    ${login_label_value1}${env}${login_label_value4}

Verify Email Field
    Wait Until Element Is Visible    ${email_field}

Verify Password Field
    Wait Until Element Is Visible    ${password_field} 

Verify Continue Button
    Wait Until Element Is Visible    ${continue_button}

Verify Error Message
    Wait Until Element Is Visible    ${error_label}
    Element Text Should Be           ${error_label}    ${error_label_value}

Verify Forgot Password Link
    Wait Until Element Is Visible    ${forgot_password_label}

###############
### Actions ###
###############

Login Page Opened
    Verify Logo
    Verify Welcome Message
    Verify Login Label
    Verify Email Field
    Verify Password Field
    Verify Continue Button
    Verify Forgot Password Link

Click Forgot Password link
    Click Element                    ${forgot_password_label}

Input Email 
    [Arguments]                      ${email}
    Input Text                       ${email_field}          ${email}

Input Password
    [Arguments]                      ${password}
    Input Text                       ${password_field}       ${password}

Click Continue Button
    Click Element                    ${continue_button}