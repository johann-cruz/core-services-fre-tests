*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${filter_modal_header_label}                               xpath: //h3[normalize-space()='Sort and Filter']
${filter_modal_header_value}                               Sort and Filter

# Sort by
${filter_modal_sort_by_label}                              xpath: //h4[normalize-space()='Sort by']
${filter_modal_sort_by_value}                              Sort by
${filter_modal_expiring_first_label}                       xpath: //span[normalize-space()='Expiring first']
${filter_modal_expiring_first_value}                       Expiring first
${filter_modal_name_a_to_z_label}                          xpath: //span[normalize-space()='Name: A to Z']    
${filter_modal_name_a_to_z_value}                          Name: A to Z
${filter_modal_name_z_to_a_label}                          xpath: //span[normalize-space()='Name: Z to A']
${filter_modal_name_z_to_a_value}                          Name: Z to A
${filter_modal_newest_first_label}                         xpath: //span[normalize-space()='Newest first']
${filter_modal_newest_first_value}                         Newest first
${filter_modal_oldest_first_label}                         xpath: //span[normalize-space()='Oldest first']
${filter_modal_oldest_first_value}                         Oldest first

# Status
${filter_modal_status_label}                               xpath: //h4[normalize-space()='Status']
${filter_modal_status_value}                               Status
${filter_modal_all_statuses_label}                         xpath: //span[normalize-space()='All Statuses']
${filter_modal_all_statuses_value}                         All Statuses
${filter_modal_actual_label}                               xpath: //span[normalize-space()='Actual']
${filter_modal_actual_value}                               Actual
${filter_modal_stopped_label}                              xpath: //span[@role='button'][normalize-space()='Stopped']
${filter_modal_stopped_value}                              Stopped
${filter_modal_obsolete_label}                             xpath: //span[normalize-space()='Obsolete']
${filter_modal_obsolete_value}                             Obsolete
${filter_modal_coming_soon_label}                          xpath: //span[normalize-space()='Coming soon']
${filter_modal_coming_soon_value}                          Coming soon
${filter_modal_configuration_required_label}               xpath: //span[normalize-space()='Configuration required']
${filter_modal_configuration_required_value}               Configuration required
${filter_modal_route_calculation_in_progress_label}        xpath: //span[normalize-space()='Route calculation in progress']
${filter_modal_route_calculation_in_progress_value}        Route calculation in progress

# Forecast Admin
${filter_modal_forecast_admin_label}                       xpath: //h4[normalize-space()='Forecast Admin']
${filter_modal_forecast_admin_value}                       Forecast Admin
${filter_modal_missing_financial_information_button}       css: label:nth-of-type(1) > .mg-check-box-root.p-2 > .mg-check-box-label > .mg-check-box-input
${filter_modal_missing_financial_information_label}        css: label:nth-of-type(1) > .mg-form-control-label-label
${filter_modal_missing_financial_information_text}         Missing financial information
${filter_modal_unverified_financial_information_button}    css: label:nth-of-type(2) > .mg-check-box-root.p-2 > .mg-check-box-label > .mg-check-box-input
${filter_modal_unverified_financial_information_label}     css: label:nth-of-type(2) > .mg-form-control-label-label
${filter_modal_unverified_financial_information_text}      Unverified financial information

# Buttons
${filter_modal_reset_all_button}                           xpath: //span[normalize-space()='Reset All']
${filter_modal_apply_button}                               xpath: //button[@class='mg-button mg-button-secondary']
${filter_modal_apply_button_text}                          APPLY
${filter_modal_close_button}                               xpath: //button[@class='mg-icon-button-root']//*[local-name()='svg']


*** Keywords ***
#############################
### Verify objects exists ###
#############################

Verify Sort and Filter Header
    Wait Until Element Is Visible    ${filter_modal_header_label}
    Element Text Should Be           ${filter_modal_header_label}    ${filter_modal_header_value}

# Sort By
Verify Sort By Header
    Wait Until Element Is Visible    ${filter_modal_sort_by_label}
    Element Text Should Be           ${filter_modal_sort_by_label}    ${filter_modal_sort_by_value}

Verify Expiring First
    Wait Until Element Is Visible    ${filter_modal_expiring_first_label}
    Element Text Should Be           ${filter_modal_expiring_first_label}    ${filter_modal_expiring_first_value}

Verify Name A to Z
    Wait Until Element Is Visible    ${filter_modal_name_a_to_z_label}
    Element Text Should Be           ${filter_modal_name_a_to_z_label}    ${filter_modal_name_a_to_z_value}

Verify Name Z to A
    Wait Until Element Is Visible    ${filter_modal_name_z_to_a_label}
    Element Text Should Be           ${filter_modal_name_z_to_a_label}    ${filter_modal_name_z_to_a_value}

Verify Newest First
    Wait Until Element Is Visible    ${filter_modal_newest_first_label}
    Element Text Should Be           ${filter_modal_newest_first_label}    ${filter_modal_newest_first_value}

Verify Oldest First
    Wait Until Element Is Visible    ${filter_modal_oldest_first_label}
    Element Text Should Be           ${filter_modal_oldest_first_label}    ${filter_modal_oldest_first_value}

# Status

Verify Status Header
    Wait Until Element Is Visible    ${filter_modal_status_label}
    Element Text Should Be           ${filter_modal_status_label}    ${filter_modal_status_value}

Verify All Statuses
    Wait Until Element Is Visible    ${filter_modal_all_statuses_label}
    Element Text Should Be           ${filter_modal_all_statuses_label}    ${filter_modal_all_statuses_value}

Verify Actual
    Wait Until Element Is Visible    ${filter_modal_actual_label}
    Element Text Should Be           ${filter_modal_actual_label}    ${filter_modal_actual_value}

Verify Stopped
    Wait Until Element Is Visible    ${filter_modal_stopped_label}
    Element Text Should Be           ${filter_modal_stopped_label}    ${filter_modal_stopped_value}

Verify Obsolete
    Wait Until Element Is Visible    ${filter_modal_obsolete_label}
    Element Text Should Be           ${filter_modal_obsolete_label}    ${filter_modal_obsolete_value}

Verify Coming soon
    Wait Until Element Is Visible    ${filter_modal_coming_soon_label}
    Element Text Should Be           ${filter_modal_coming_soon_label}    ${filter_modal_coming_soon_value}

Verify Configuration required
    Wait Until Element Is Visible    ${filter_modal_configuration_required_label}
    Element Text Should Be           ${filter_modal_configuration_required_label}    ${filter_modal_configuration_required_value}

Verify Route Calculation In Progress
    Wait Until Element Is Visible    ${filter_modal_route_calculation_in_progress_label}
    Element Text Should Be           ${filter_modal_route_calculation_in_progress_label}    ${filter_modal_route_calculation_in_progress_value}

# Forecast Admin

Verify Forecast Admin Header
    Wait Until Element Is Visible    ${filter_modal_forecast_admin_label}
    Element Text Should Be           ${filter_modal_forecast_admin_label}    ${filter_modal_forecast_admin_value}

Verify Missing Financial Information
    Wait Until Page Contains Element    ${filter_modal_missing_financial_information_button}
    Wait Until Element Is Visible       ${filter_modal_missing_financial_information_label}
    ${value}=          Get Text         ${filter_modal_missing_financial_information_label}
    Should Be Equal As Strings          ${value}    ${filter_modal_missing_financial_information_text}

Verify Unverified Financial Information
    Wait Until Page Contains Element    ${filter_modal_unverified_financial_information_button}
    Wait Until Element Is Visible       ${filter_modal_unverified_financial_information_label}
    ${value}=          Get Text         ${filter_modal_unverified_financial_information_label}
    Should Be Equal As Strings          ${value}    ${filter_modal_unverified_financial_information_text}

Verify Apply Button
    Wait Until Element Is Visible    ${filter_modal_apply_button}
    Element Text Should Be           ${filter_modal_apply_button}    ${filter_modal_apply_button_text}

Verify Close Button
    Wait Until Element Is Visible    ${filter_modal_close_button}
    Element Should Be Enabled        ${filter_modal_close_button}

###############
### Actions ###
###############

Click Sort And Filter
    [Arguments]       ${sort_by_filter}   ${status_filter}    ${forecast_admin_filter}=${None}
    ${is_reset_all_button_visible}=      Get Element Count  ${filter_modal_reset_all_button}
    Run Keyword If    ${is_reset_all_button_visible} > 0    Click Reset All Button
    Wait Until Element Is Visible    ${sort_by_filter}
    Click Element     ${sort_by_filter}
    Wait Until Element Is Visible    ${status_filter}
    Click Element     ${status_filter}
    Run Keyword If    '${forecast_admin_filter}' == 'Missing financial information'        Click Missing Financial Information Checkbox   
    ...  ELSE IF      '${forecast_admin_filter}' == 'Unverified financial information'     Click Unverified Financial Information Checkbox 
    Click Button      ${filter_modal_apply_button}

Click Missing Financial Information Checkbox
    Click Element     ${filter_modal_missing_financial_information_button}

Click Unverified Financial Information Checkbox
    Click Element     ${filter_modal_unverified_financial_information_button}

Click Reset All Button
    Click Element    ${filter_modal_reset_all_button}

Click Close Button
    Click Element    ${filter_modal_close_button}