*** Settings ***
Library    SeleniumLibrary


*** Variables ***
###Headers###
${forecast_url}                    https://d3hljngs5u3bb6.cloudfront.net/subscriptions/page/1
${forecast_label}                  xpath: //h1[contains(text(),'Forecasts')]
${forecast_label_value}            Forecasts
${filter_button}                   css: .mg-button.mg-button-secondary.ml-4
${filter_button_label}             class: mg-button-label
${forecast_add_button}             css: button[class='mg-button mg-button-secondary mr-4']

###Tiles###
${forecast_first_tile}             css: div:nth-of-type(1) > .d-flex.flex-column.justify-content-start.mg-subscription-entry
${forecast_first_tile_ellipsis}    css: div:nth-of-type(1) > .d-flex.flex-column.justify-content-start.mg-subscription-entry  .mg-icon-button-root.mg-subscription-entry-actions > svg
${forecast_first_tile_link}        css: div:nth-of-type(1) > .d-flex.flex-column.justify-content-start.mg-subscription-entry > .mg-tooltip-wrapper > .d-block.mg-link.mg-text-align-center.mx-3 > .mg-height-h3.mg-link

###Ellipsis###
${ellipsis_menu}                   class: mg-list-item-root
${ellipsis_view_label}             xpath: //a[normalize-space()='View']
${view_label_value}                View
${edit_label_value}                Edit
${forecast_admin_label_value}      Forecast admin
${generate_report_label_value}     Generate report
${create_copy_label_value}         Create copy
${stop_label_value}                Stop


*** Keywords ***
#############################
### Verify objects exists ###
#############################

Verify Forecast URL
    Location Should Be                    ${forecast_url}

Verify Forecast Header
    Wait Until Element Is Visible         ${forecast_label}
    Element Text Should Be                ${forecast_label}           ${forecast_label_value}

Verify Forecast First Tile
    Wait Until Page Contains Element      ${forecast_first_tile}      timeout=10
    Element Should Be Visible             ${forecast_first_tile}

Verify View
    Wait Until Element Is Visible         ${ellipsis_view_label}
    Element Text Should Be                ${ellipsis_view_label}      ${view_label_value} 

Verify ReadOnly User Ellipsis Menu
    ${count}=  Get Element Count          ${ellipsis_menu}
    Should be equal as numbers            ${count}    1  

Verify Filter Button
    Wait Until Element Is Visible         ${filter_button}
    Element Should Be Enabled             ${filter_button}

Verify Filter Btn Label
    [Arguments]                           ${filter_button_value}
    Wait Until Element Is Visible         ${filter_button}
    Element Text Should Be                ${filter_button_label}    ${filter_button_value}

Verify Forecast Add Button Is Not Present
    Page Should Not Contain Button        ${forecast_add_button}

###############
### Actions ###
###############

Hover Forecast First Tile
    Wait until Element Is Enabled         ${forecast_first_tile}
    Mouse Over                            ${forecast_first_tile}

Click Forecast Ellipsis
    Wait Until Element Is Visible         ${forecast_first_tile_ellipsis}
    Click Element                         ${forecast_first_tile_ellipsis}

Click Filter Button
    Sleep    5s
    Click Element                         ${filter_button}

Click Forecast link
    Wait Until Element Is Visible         ${forecast_first_tile_link}
    Click Element                         ${forecast_first_tile_link}