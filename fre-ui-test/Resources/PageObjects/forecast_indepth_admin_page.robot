*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${forecast_admin_header_label}         xpath: //h3[normalize-space()='Forecast Admin']
${forecast_admin_header_value}         Forecast Admin
${customer_id_label}                   xpath: //span[normalize-space()='Customer ID:']
${customer_id_value}                   Customer ID:
${customer_name_label}                 xpath: //span[normalize-space()='Customer name:']
${customer_name_value}                 Customer name:
${type_of_contract_label}              xpath: //span[normalize-space()='Type of contract:']
${type_of_contract_value}              Type of contract:
${subscription_number_label}           xpath: //span[normalize-space()='Subscription number:']
${subscription_number_value}           Subscription number:

${product_code_label}                  xpath: //span[normalize-space()='Product code:']
${product_code_value}                  Product code:
${project_description_label}           xpath: //span[normalize-space()='Project description:']
${project_description_value}           Project description:
${project_id_label}                    xpath: //span[normalize-space()='Project ID:']
${project_id_value}                    Project ID:
${relates_to_project_label}            xpath: //span[normalize-space()='Relates to project:']
${relates_to_project_value}            Relates to project:

${billing_status_label}                xpath: //span[normalize-space()='Billing status:']
${billing_status_value}                Billing status:
${sf_contract_id_label}                xpath: //span[normalize-space()='SF Contract ID:']
${sf_contract_id_value}                SF Contract ID:
${type_of_sf_contract_label}           xpath: //span[normalize-space()='Type of SF contract:']
${type_of_sf_contract_value}           Type of SF contract:
${contract_start_date_label}           xpath: //span[normalize-space()='Contract start date:']
${contract_start_date_value}           Contract start date:
${contract_end_date_label}             xpath: //span[normalize-space()='Contract end date:']
${contract_end_date_value}             Contract end date:
${erp_reference_label}                 xpath: //span[normalize-space()='ERP reference:']
${erp_reference_value}                 ERP reference:
${billing_entity_of_customer_label}    xpath: //span[normalize-space()='Billing entity of customer:']
${billing_entity_of_customer_value}    Billing entity of customer:
${po_number_label}                     xpath: //span[normalize-space()='PO-number:']
${po_number_value}                     PO-number:
${customer_contact_label}              xpath: //span[normalize-space()='Customer contact:']
${customer_contact_value}              Customer contact:
${invoicing_method_label}              xpath: //span[normalize-space()='Invoicing method:']
${invoicing_method_value}              Invoicing method:
${billing_entity_label}                xpath: //span[normalize-space()='Billing entity:']
${billing_entity_value}                Billing entity:
${price_label}                         xpath: //span[normalize-space()='Price:']
${price_value}                         Price:
${currency_label}                      xpath: //span[normalize-space()='Currency:']
${currency_value}                      Currency:
${cross_sell_with_dtn_llc_label}       xpath: //span[normalize-space()='Cross sell with DTN LLC:']
${cross_sell_with_dtn_llc_value}       Cross sell with DTN LLC:
${imo_number_label}                    xpath: //span[normalize-space()='IMO number:']
${imo_number_value}                    IMO number:
${verification_status_label}           xpath: //span[normalize-space()='Verification status:']
${verification_status_value}           Verification status:


*** Keywords ***
Verify Forecast Admin Header
    Wait Until Element Is Visible    ${forecast_admin_header_label}
    Element Text Should Be           ${forecast_admin_header_label}    ${forecast_admin_header_value}

Verify Customer ID
    Wait Until Element Is Visible    ${customer_id_label}
    Element Text Should Be           ${customer_id_label}    ${customer_id_value}

Verify Customer Name
    Wait Until Element Is Visible    ${customer_name_label}
    Element Text Should Be           ${customer_name_label}    ${customer_name_value}

Verify Type Of Contract
    Wait Until Element Is Visible    ${type_of_contract_label}
    Element Text Should Be           ${type_of_contract_label}    ${type_of_contract_value}

Verify Subscription Number
    Wait Until Element Is Visible    ${subscription_number_label}
    Element Text Should Be           ${subscription_number_label}    ${subscription_number_value}

Verify Product Code
    Wait Until Element Is Visible    ${product_code_label}
    Element Text Should Be           ${product_code_label}    ${product_code_value}

Verify Project Description
    Wait Until Element Is Visible    ${project_description_label}
    Element Text Should Be           ${project_description_label}    ${project_description_value}

Verify Project ID
    Wait Until Element Is Visible    ${project_id_label}
    Element Text Should Be           ${project_id_label}    ${project_id_value}

Verify Relates To Project
    Wait Until Element Is Visible    ${relates_to_project_label}
    Element Text Should Be           ${relates_to_project_label}    ${relates_to_project_value}

Verify Billing Status
    Wait Until Element Is Visible    ${billing_status_label}
    Element Text Should Be           ${billing_status_label}    ${billing_status_value}

Verify SF Contract ID
    Wait Until Element Is Visible    ${sf_contract_id_label}
    Element Text Should Be           ${sf_contract_id_label}    ${sf_contract_id_value}

Verify Type Of SF Contract
    Wait Until Element Is Visible    ${type_of_sf_contract_label}
    Element Text Should Be           ${type_of_sf_contract_label}    ${type_of_sf_contract_value}

Verify Contract Start Date
    Wait Until Element Is Visible    ${contract_start_date_label}
    Element Text Should Be           ${contract_start_date_label}    ${contract_start_date_value}

Verify Contract End Date
    Wait Until Element Is Visible    ${contract_end_date_label}
    Element Text Should Be           ${contract_end_date_label}    ${contract_end_date_value}

Verify ERP Reference
    Wait Until Element Is Visible    ${erp_reference_label}
    Element Text Should Be           ${erp_reference_label}    ${erp_reference_value}

Verify Billing Entity Of Customer
    Wait Until Element Is Visible    ${billing_entity_of_customer_label}
    Element Text Should Be           ${billing_entity_of_customer_label}    ${billing_entity_of_customer_value}

Verify PO Number
    Wait Until Element Is Visible    ${po_number_label}
    Element Text Should Be           ${po_number_label}    ${po_number_value}

Verify Customer Contact
    Wait Until Element Is Visible    ${customer_contact_label}
    Element Text Should Be           ${customer_contact_label}    ${customer_contact_value}

Verify Invoicing Method
    Wait Until Element Is Visible    ${invoicing_method_label}
    Element Text Should Be           ${invoicing_method_label}    ${invoicing_method_value}

Verify Billing Entity
    Wait Until Element Is Visible    ${invoicing_method_label}
    Element Text Should Be           ${invoicing_method_label}    ${invoicing_method_value}

Verify Price
    Wait Until Element Is Visible    ${price_label}
    Element Text Should Be           ${price_label}    ${price_value}

Verify Currency
    Wait Until Element Is Visible    ${currency_label}
    Element Text Should Be           ${currency_label}    ${currency_value}

Verify Cross Sell With DTN LLC
    Wait Until Element Is Visible    ${cross_sell_with_dtn_llc_label}
    Element Text Should Be           ${cross_sell_with_dtn_llc_label}    ${cross_sell_with_dtn_llc_value}

Verify IMO Number With Filled
    Wait Until Element Is Visible    ${verification_status_label}
    Element Text Should Be           ${verification_status_label}    ${verification_status_value}