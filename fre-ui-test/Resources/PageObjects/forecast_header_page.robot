*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${dtn_image}                     xpath: //*[name()='g' and contains(@fill-rule,'nonzero')]
${fre_offshore_label}            xpath: //*[name()='tspan' and contains(@x,'13')]
${fre_offshore_value}            Flexible Report Engine UI — Offshore
${forecast_link}                 xpath: //a[contains(text(),'Forecasts')]
${forecast_link_value}           Forecasts
${archieve_link}                 xpath: //a[normalize-space()='Archive']
${archieve_link_value}           Archive
${products_menu}                 xpath: //p[normalize-space()='Products']
${products_menu_value}           Products
${readonly_profile_image}        xpath: //img[@alt='Readonly User']

${administration_menu}           xpath: //p[normalize-space()='Administration']
${administration_menu_value}     Administration
${distribution_list_link}        xpath: //a[normalize-space()='Distribution List']
${distribution_list_link_value}  Distribution List
${ftp_servers_list_link}         xpath: //a[normalize-space()='FTP Servers List']
${ftp_servers_list_link_value}   FTP Servers List
${customers_link}                xpath: //a[normalize-space()='Customers']
${customers_link_value}          Customers
${templates_link}                xpath: //a[normalize-space()='Templates']
${templates_link_value}          Templates
${manual_autotext_link}          xpath: //a[normalize-space()='Manual Autotext']
${manual_autotext_link_value}    Manual Autotext

${profile_menu}                  xpath: //div[@class='mg-user-profile d-flex align-items-center']//*[local-name()='svg']
${profile_name}                  css: div[role='tooltip'] p:nth-child(1)
${profile_email}                 css: p[class='mg-body-2-gray mt-1']
${sign_out_link}                 xpath: //a[normalize-space()='Sign Out']
${sign_out_link_value}           Sign Out

*** Keywords ***
Verify DTN Image
    Page Should Contain Element      ${dtn_image}

Verify FRE UI - Offshore 
    Wait Until Element Is Visible    ${fre_offshore_label}
    Element Text Should Be           ${fre_offshore_label}    ${fre_offshore_value}

Verify Forecast Link
    Wait Until Element Is Visible    ${forecast_link}
    Element Text Should Be           ${forecast_link}         ${forecast_link_value}

Verify Administration Menu 
    Wait Until Element Is Visible    ${administration_menu}
    Element Text Should Be           ${administration_menu}         ${administration_menu_value}