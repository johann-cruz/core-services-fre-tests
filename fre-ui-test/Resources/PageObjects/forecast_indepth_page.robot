*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${financial_info_forecast_admin_label}    xpath: //a[contains(text(),'Forecast Admin')]

*** Keywords ***
Verify ForeCast Admin Link Is Not Present
    Page Should Not Contain Button        ${financial_info_forecast_admin_label}