***Variables***

# browsers and url address
${browser}                  Chrome
&{home_url}                 TEST=https://d3hljngs5u3bb6.cloudfront.net  PROD=https://d2t5j0h2hch8yf.cloudfront.net
${env}                      TEST

# user acounts
${password_valid}           Welcome2020!
${email_superadmin}         johann.cruz@dtn.com
${email_admin}              admin@test.com
${email_readonly}           readonly@test.com
${email_finance}            finance@test.com 

${email_invalid}            invalid@dtn.com
${password_invalid}         Welcome2020
${dummy_email}              automation@dtn.com