*** Settings ***
Library     SeleniumLibrary
Resource    ../TestData/config_data.robot

*** Keywords ***
Start TestCase
    [Documentation]             This will open the browser
    Open Browser                ${home_url.${env}}  ${browser}         options=add_argument("--ignore-certificate-errors");add_experimental_option('excludeSwitches', ['enable-logging'])                
    Maximize Browser Window

Finish TestCase
    [Documentation]             This will close the browser
    Close Browser